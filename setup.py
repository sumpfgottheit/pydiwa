from setuptools import setup

setup(
    name='pydiwa',
    version='1.0',
    py_modules=['pydiwa'],
    include_package_data=True,
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        pydiwa=cli:cli
    '''
)
