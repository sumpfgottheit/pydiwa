# -*- coding: utf-8 -*-
__author__ = 'saf'

from flask import Flask, render_template, flash, redirect, url_for, request
from flask_wtf import Form
from wtforms.fields import IntegerField, BooleanField, StringField
from wtforms.validators import InputRequired
import logging
from util import setup_logging, writefile
from project import DiwaContainer

app = Flask(__name__)
setup_logging()

app.config.update(
    SECRET_KEY='ASDF9qKd937fzzrt23rd',
)

from project import Umbrella, DiwaError
umbrella = Umbrella()

def get_project_container_from_umbrella(project, container):
    umbrella.read_projects()
    _project = umbrella.projects.get(project, None)
    if _project is None:
        raise DiwaError("%s is not a Project" % project)
    _container = _project.get_container(container)
    if _container is None:
        raise DiwaError("%s is not a Container " % _container)
    return _project, _container

def get_project_from_umbrella(project):
    umbrella.read_projects()
    _project = umbrella.projects.get(project, None)
    if _project is None:
        raise DiwaError("%s is not a Project" % project)
    return _project

@app.errorhandler(404)
def error_404(error):
    message = "Error 404. Url %s not valid." % request.base_url
    flash(message, category='error')
    return redirect(url_for('index'))

@app.errorhandler(DiwaError)
def error_diwa(error):
    flash(str(error), category='error')
    return redirect(url_for('index'))


@app.route('/')
def index():
    return redirect(url_for('projects'))

@app.route('/projects')
def projects():
    umbrella.read_projects()
    return render_template('projects.html', projects=umbrella.projects)

@app.route('/projects/<project>')
def project_detail(project):
    _project = get_project_from_umbrella(project)
    return render_template('project.html', project=_project)

@app.route('/projectactions/<project>/<action>', methods=('GET', 'POST'))
def project_action(project, action):
    _project = get_project_from_umbrella(project)
    try:
        if action == 'up':
            _project.up()
            flash("Started Project %s (docker-compose-up))" % project, category='success')
            umbrella.read_projects()
        elif action == 'stop':
            _project.stop()
            flash("Stopped Project %s. (docker-compose stop)" % project, category='success')
            umbrella.read_projects()
        elif action == 'addcontainer':
            return container_add(_project)
    except Exception as e:
        flash('%s of Project %s failed' % (action, project), category='error')
        flash(str(e), category='error')
    return redirect(url_for('project_detail', project=project))


def container_add_form_factory(project):
    class F(Form):
        pass
    used_classes = [c.__class__ for c in project.containers]
    available_classes = []
    for c in DiwaContainer.__subclasses__():
        if c not in used_classes:
            available_classes.append(c)
    for c in available_classes:
        setattr(F, c.__name__, BooleanField(label="%s :: %s" % (c.__name__, c.description), default=''))
    return F, used_classes

def container_add(project):
    Form, used_classes = container_add_form_factory(project)
    form = Form()
    if form.validate_on_submit():
        for container_class, val in form.data.items():
            if not val:
                continue
            ContainerClass = DiwaContainer.get_class_for_name(container_class)
            if ContainerClass is None:
                flash("No Container %s available." % container_class, category='error')
            else:

                container = ContainerClass(project.abspath)
                container.compose_project = project.compose_project
                containers_to_recreate = project.add_container(container)
                flash('Addect new Container %s' % ContainerClass.__name__, category='success')
                if len(containers_to_recreate) > 0:
                    container_names = ", ".join([container.name for container in containers_to_recreate])
                    flash('The following containers needed to be recreated: %s.' % container_names, category='info')
                    for container in containers_to_recreate:
                        container.recreate()
                try:
                    if not container.volumes_are_empty:
                        flash("Existing Volumes found. Volumes will be reused.")
                except OSError as e:
                    logging.warn(str(e))
                    flash("Could not evalute state of existing volumes. If volumes for the container %s exist within the directory %s, they will be reused." % (container.name, container.volume_dir), category='warning')
            umbrella.read_projects()
            logging.info(form.data)
        return redirect(url_for('project_detail', project=project.name))
    else:
        return render_template('container_add.html', form=form, project=project, used_classes=used_classes )


#
# Container Acation
#

@app.route('/projects/<project>/<container>/<action>', methods=('GET', 'POST'))
def container_action(project, container, action):
    _project, _container = get_project_container_from_umbrella(project, container)
    if action == 'logs':
        return render_template('container_logs.html', project=_project, container=_container)
    try:
        if action == 'start':
            _container.start()
            flash('Container %s started' % container, category='success')
        elif action == 'stop':
            _container.stop()
            flash('Container %s stopped' % container, category='success')
        elif action == 'editports':
            return container_ports(_project, _container)
        elif action == 'remove':
            return render_template('container_remove.html', project=_project, container=_container)
        elif action == 'reallyremove':
            containers_to_recreate = _project.remove_container(_container.name)
            flash('Container %s removed and stopped from project %s. Volumes have NOT been deleted.' % (_container.name, _project.name), category='success')
            if len(containers_to_recreate) > 0:
                container_names = ", ".join([container.name for container in containers_to_recreate])
                flash('The following containers needed to be recreated: %s.' % container_names, category='info')
                for container in containers_to_recreate:
                    container.recreate()
            _project.write_docker_compose_file()
        else:
            flash("Unknown Action. You may have been funny and test different urls. If not, than it's my fault.", category='warning')
    except Exception as e:
        flash('%s of container %s failed' % (action, container), category='error')
        flash(str(e), category='error')
    return redirect(url_for('project_detail', project=project))

def container_ports_form_factory(container):
    class F(Form):
        pass
    for port in container.ports:
        value = int(container.public_ports.get(str(port), 0))
        setattr(F, "port_%s" % port, IntegerField(default=value, label="Port %s" % port))
    return F

def container_ports(project, container):
    Form = container_ports_form_factory(container)
    form = Form()
    if form.validate_on_submit():
        new_ports = dict([(str(int(container_port[5:])), str(host_port)) for container_port, host_port in form.data.items() if container_port.startswith('port_') and host_port != 0])
        logging.info("new_ports: %s" % str(new_ports))
        logging.info("current_ports; %s" % str(container.public_ports))
        if container.public_ports == new_ports:
            flash("No Changes.")
        else:
            running = container.is_running
            if running:
                container.stop()
            container.public_ports = new_ports
            project.write_docker_compose_file()
            umbrella.read_projects()
            project = umbrella.projects[project.name]
            container = project.get_container(container.name)
            flash("New Port Configuration saved.", category='success')
            if running:
                container.recreate()
                container.start()
                flash("Container has been restarted.", category='success')

        return redirect(url_for('project_detail', project=project.name))
    else:
        return render_template('container_ports.html', form=form, project=project, container=container)

#
# New Project
#
@app.route('/newproject', methods=('GET', 'POST'))
def project_new():
    class F(Form):
        projectname = StringField(label="Projectname", validators=[InputRequired()])
    umbrella.read_projects()
    form = F()
    if form.validate_on_submit():
        flash(form.data)
        name = form.data.get('projectname')
        try:
            umbrella.create_project(name)
        except Exception as e:
            flash(str(e), category='error')
            if app.debug:
                raise e
            return render_template('project_new.html', form=form)
        return redirect(url_for('project_detail', project=name))
    else:
        return render_template('project_new.html', form=form)

#
# Show Sample Sourcecode
#
@app.route('/sample/<project>/<container>')
def sample_code(project, container):
    _project, _container = get_project_container_from_umbrella(project, container)
    return render_template('sample_source.html', project=_project, container=_container)


@app.route('/writelivedemo/<project>/<container>')
def write_live_demo(project, container):
    _project, _container = get_project_container_from_umbrella(project, container)
    filename, content = _container.sample_file
    try:
        writefile(filename, content)
        flash("Livedemo File has been written to %s. You need to navigate to the written file according to the Port and other Configurations." % filename)
    except IOError as e:
        flash(str(e), category='error')
    return render_template('project.html', project=_project)

@app.route('/settings')
def settings():
    d = {'diwa_umbrella_dir': umbrella.dir}
    return render_template('settings.html', **d)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
