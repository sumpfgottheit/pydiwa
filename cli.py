#!/usr/bin/env python
# -*- coding: utf-8 -*-



import click
from project import *
import logging
from os.path import isfile
from util import exitn, setup_logging



@click.group()
@click.version_option()
def cli():
    setup_logging()
    logging.debug("STARTING UP")

#
# CREATE
#

@cli.command()
@click.option('--name', '-n')
@click.option('--directory', '-d', default='.')
def create(name, directory):
    project = DiwaProject(name, directory)
    elk = project.add_container(ElkContainer)
    project.ensure_directory_structure()
    project.write_docker_compose_file()
#
# READ
#

@cli.command()
@click.option('--file', '-f', default='docker-compose.yml')
def read(file):
    if not isfile(file):
        exitn("Configfile %s not found." % file)
    project = DiwaProject.from_docker_compose_file(file)
    print project.yaml_for_docker_compose

#
# TEST
#

@cli.group()
def test():
    pass

@test.command('yaml')
def test():
    p = DiwaProject('/home/saf/diwa/projects/sessel')
    elk = p.add_container(ElkContainer)
    pg = p.add_container(Postgres94Container)
    pg.password = 'rababer'
    print p.yaml_for_docker_compose
