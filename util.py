# -*- coding: utf-8 -*-
__author__ = 'saf'

import click
import sys
from os.path import isdir
from os import chmod, makedirs, listdir
import random
import string
import logging
import graypy
import hashlib

def is_empty(item):
    if isinstance(item, list) and len(item) == 0:
        return True
    if isinstance(item, basestring) and item == '':
        return True
    return False

def exitn(message='', exitcode=1):
    click.echo(message)
    if exitcode > 0:
        click.echo(click.style("Exitcode %d" % exitcode, fg='red'))
    sys.exit(exitcode)

def ensure_dirs(path, mode=0775):
    if not isdir(path):
        makedirs(path)
    chmod(path, mode)

def random_password(digits=15):
    return ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(digits))

def setup_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(graypy.GELFHandler('localhost', 12201))
    formatter = logging.Formatter( "%(asctime)s - %(name)s - [%(funcName)s]:%(lineno)3d - %(levelname)s - %(message)s")
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

def is_string(s):
    return isinstance(s, basestring)

def sha512sum(filename):
    with open(filename) as f:
        h = hashlib.sha512(f.read()).hexdigest()
    return h

def directory_is_empty(directory):
    return len(listdir(directory)) > 0

def readfile(filename):
    with open(filename) as f:
        content = f.read()
    return content

def writefile(filename, content):
    with open(filename, 'w') as f:
        f.write(content)
    f.close()
