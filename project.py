# -*- coding: utf-8 -*-
__author__ = 'saf'

from os import getcwd, chdir, listdir, environ, mkdir
from os.path import isdir, join, isfile, dirname, basename, abspath
import yaml
from util import is_empty, ensure_dirs, random_password, sha512sum, directory_is_empty, readfile
import logging
import re
from random import SystemRandom
from compose.cli.command import Command
from parser import ParserError
from compose.cli.errors import UserError
from jinja2 import Environment, FileSystemLoader

DOCKER_COMPOSE_FILE = 'docker-compose.yml'
CONTAINERNAME_ELK = 'elk'
CONTAINERNAME_APACHEPHP = 'apache'
CONTAINERNAME_POSTGRES94 = 'postgres94'

class DiwaError(Exception):
    pass

class Umbrella(object):

    def __init__(self, umbrelladir=None):
        self.dir = umbrelladir
        if self.dir is None:
            self.dir = environ.get('DIWA_UMBRELLA_DIR', None)
        if self.dir is None:
            raise DiwaError("Environment Variable DIWA_UMBRELLA_DIR not set")
        if not isdir(self.dir):
            raise DiwaError("DIWA_UMBRELLA_DIR %s is not a directory" % self.dir)
        logging.info("** Umbrella in %s created" % self.dir)
        self.projects = {}
        self.read_projects()


    def read_projects(self):
        chdir(self.dir)
        self.projects = {}
        for dir in listdir(self.dir):
            if DiwaProject.is_diwaproject_dir(dir):
                project_file = abspath(join(self.dir, dir, DOCKER_COMPOSE_FILE))
                project = self.projects.get(dir, None)
                if project is not None and project.sha512_original == sha512sum(project_file):
                    logging.info("Project %s did not change" % project.name)
                else:
                    try:
                        self.projects[dir] = DiwaProject.from_docker_compose_file(project_file)
                        logging.info("Project %s reread" % dir)
                    except AttributeError as e:
                        logging.error("%s is not a Diwa Project" % dir)

    def create_project(self, name):
        chdir(self.dir)
        if name in self.project_names:
            raise DiwaError('Project with name %s already exists.' % name)
        mkdir(name)
        project = DiwaProject(name, join(self.dir, name), DOCKER_COMPOSE_FILE)
        self.projects[name] = project

    @property
    def project_names(self):
        return self.projects.keys()


class DiwaProject(object):
    """Umbrella class for the Project. Reads/Writes the Docker-compose file"""

    @classmethod
    def is_diwaproject_dir(cls, path):
        return isdir(path) and isfile(join(path, DOCKER_COMPOSE_FILE))

    @classmethod
    def from_docker_compose_file(cls, filename):
        logging.info("Create Project from file %s" % filename)
        name = basename(dirname(filename))
        if not name:
            raise SyntaxError("Path to docker-compose file needs to include the project-directory. Eg: garden/docker-compose.yml. But the filename was %s" % filename)
        project = DiwaProject(name, dirname(filename), basename(filename))
        with open(filename) as f:
            content = yaml.load(f)
            for container_name, config in content.items():
                container = DiwaContainer.create_from_hash(container_name, config, project)
                container.compose_project = project.compose_project
                project.add_container(container)
        logging.info("Project %s created" % project.name)
        return project

    def __init__(self, name, path, filename):
        self.name = name
        self.path = getcwd() if path is None else path
        self.containers = []
        self.filename = filename
        self._command = Command()
        try:
            self.compose_project = self._command.get_project(join(self.path, self.filename))
            self.sha512_original = sha512sum(join(self.abspath, self.filename))
        except (OSError, ParserError, UserError):
            self.compose_project = None
            self.sha512_original = 0
            self.write_docker_compose_file()

    def ensure_directory_structure(self):
        logging.info("Ensuring directories. Projectdirectory of project %s is %s" % (self.name, self.path))
        ensure_dirs(self.path)
        chdir(self.path)
        ensure_dirs('_logs')
        for container in self.containers:
            container.ensure_directories()

    def add_container(self, container):
        logging.info("Add container %s to project" % container.name)
        container_class = container.__class__
        for _container in self.containers:
            if _container.__class__ is container_class:
                raise DiwaError("Already a container of class %s defined" % container_class.__name__)
        containers_to_recreate = container.add_self_to_project(self.containers)
        self.containers.append(container)
        for _container in self.containers[:-1]:
            _container.added_to_project(self.containers)
        self.write_docker_compose_file()
        return containers_to_recreate if containers_to_recreate is not None else []

    def remove_container(self, name):
        logging.info("Remove container %s from project" % name)
        container = self.get_container(name)
        if container is None:
            raise DiwaError("No Container named %s to remove found" % name)
        self.containers.remove(container)
        containers_to_recreate = container.remove_self_from_project(self.containers)
        for _container in self.containers:
            _container.removed_from_project(self.containers, container)
        container.stop()
        container.compose_service.remove_stopped()
        self.write_docker_compose_file()
        return containers_to_recreate if containers_to_recreate is not None else []

    def get_container(self, name):
        for container in self.containers:
            if container.name == name:
                return container
        return None

    def has_container(self, name):
        return self.get_container(name) is not None

    @property
    def containers_running(self):
        l = [c.is_running for c in self.containers]
        return l.count(True)

    @property
    def containers_stopped(self):
        l = [c.is_running for c in self.containers]
        return l.count(False)

    @property
    def abspath(self):
        return abspath(self.path)

    @property
    def abspath_docker_compose_file(self):
        return join(self.abspath, self.filename)

    @property
    def has_containers(self):
        return len(self.containers) > 0

    def up(self):
        logging.info("Restarting Project %s." % self.name)
        chdir(self.abspath)
        if self.has_containers:
            self.compose_project.up(detach=True)

    def stop(self):
        logging.info("Stopping Project %s." % self.name)
        chdir(self.abspath)
        if self.has_containers:
            self.compose_project.stop()
            self.compose_project.remove_stopped()

    @property
    def public_ports(self):
        p = []
        for container in self.containers:
            p.extend(container.public_ports.values())
        return p

    @property
    def hash_for_docker_compose(self):
        h = {}
        for container in self.containers:
            h.update(container.hash_for_docker_compose)
        return h

    @property
    def yaml_for_docker_compose(self):
        return yaml.dump(self.hash_for_docker_compose, indent=2, default_flow_style=False).replace(' - ', '   - ')

    def write_docker_compose_file(self):
        filename = join(self.abspath, self.filename)
        logging.info("Writing File %s" % filename)
        with open(filename, 'w') as f:
            f.write(self.yaml_for_docker_compose)
        if self.compose_project is None:
            self.compose_project = self._command.get_project(join(self.path, self.filename))


class ForeignContainer(object):
    """A Container, that is not known to Diwa"""
    def __init__(self, name, h):
        logging.info("New Foreigncontainer named %s" % name)
        self.name = name
        self.h = h

    @property
    def hash_for_docker_compose(self):
        return {self.name: self.h}

class DiwaContainer(object):

    @classmethod
    def get_class_for_image(cls, image):
        """Return the class which uses the given image or None"""
        for c in cls.__subclasses__():
            if c.image == image:
                return c
        return None

    @classmethod
    def get_class_for_name(cls, name):
        """Return the class which uses the given image or None"""
        for c in cls.__subclasses__():
            if c.__name__ == name:
                return c
        return None

    @classmethod
    def create_from_hash(cls, container_name, config, project):
        if 'MANAGED_BY_DIWA=FALSE' in [c.upper() for c in config.get('environment', [])]:
            logging.info("Container %s is explicitely not managed by diwa" % container_name)
            return ForeignContainer(container_name, config)
        image = config.get('image', None)
        container_class = cls.get_class_for_image(image)
        if container_class is None:
            logging.info("Container named %s with image %s is not a Diwacontainer" % (container_name, image))
            return ForeignContainer(container_name, config)
        logging.info("Container named %s with image %s is a %s" % (container_name, image, container_class.__name__))
        container = container_class(project)
        del(config['image'])
        container.configure_from_hash(config)
        return container

    _logs_dir = '_logs'
    ports = []
    description = ""
    is_devel_container = False

    def __init__(self, name, image, project):
        logging.info("New %s named %s" % (self.__class__.__name__, name))
        self.name = name
        self.image = image
        self.project = project
        self.volume_dir = "%s/volumes" % self.name
        self.links = []
        self.host_log_dir = "%s/%s" % (DiwaContainer._logs_dir, self.name)
        self.volumes = ["%s:/logs" % self.host_log_dir, ]
        self.unkown_docker_compose_items = {}
        self.environment = {}
        self.ports = self.__class__.ports
        self.public_ports = {}
        self.description = self.__class__.description
        self.hostname = name
        self.compose_project = None

    def __trim_volumes(self):
        _volumes = []
        for volume in self.volumes:
            _volumes.append(":".join([path.rstrip('/') for path in volume.split(':')]))
        self.volumes = list(sorted(set(_volumes)))

    @property
    def project_dir(self):
        return self.project.abspath

    @property
    def volumes_are_empty(self):
        dirs = [join(self.project_dir, v.split(':')[0]) for v in self.volumes]
        non_empty_dirs = [d for d in dirs if not directory_is_empty(d)]
        return len(non_empty_dirs) == 0

    @property
    def compose_service(self):
        if self.compose_project:
            return self.compose_project.get_service(self.name)
        else:
            return None

    @property
    def compose_container(self):
        if len(self.compose_service.containers()) == 0:
            return None
        else:
            return self.compose_service.containers()[0]

    @property
    def is_running(self):
        return self.compose_container is not None

    def start(self):
        chdir(self.project_dir)
        if not self.is_running:
            return self.compose_service.start_or_create_containers()

    def stop(self):
        chdir(self.project_dir)
        if self.is_running:
            return self.compose_service.stop()
        self.compose_service.remove_stopped()

    def recreate(self):
        chdir(self.project_dir)
        running = self.is_running
        if running:
            self.compose_service.recreate_container(self.compose_container)
        else:
            self.compose_service.remove_stopped()

    @property
    def logs(self):
        if self.is_running:
            return self.compose_container.logs()
        else:
            return None
    @property
    def public_ports_list(self):
        return list(sorted(["%s:%s" % (host_port, container_port) for container_port, host_port in self.public_ports.items()]))

    @property
    def environment_list(self):
        return sorted(["%s=%s" % (key, value) for key, value in self.environment.items()])

    def configure_from_hash(self, h):
        """Take the hash, which has been imported from a docker-compose.yml and configure the
        container object accordingly
        """
        environment = h.pop('environment', [])
        for line in environment:
            splitted = re.split(r'(:| |=)+', line)
            if len(splitted) == 0:
                continue
            key = splitted[0]
            val = '' if len(splitted) == 1 else splitted[-1]
            self.environment[key] = val
        self.hostname = h.pop('hostname', self.hostname)
        self.volumes.extend(h.pop('volumes', []))
        self.__trim_volumes()
        for pairs in h.pop('ports', []):
            if pairs.count(':') != 1:
                logging.warn('Unknown Port definition for container %s: %s' % (self.name, pairs))
            else:
                host_port, port = pairs.split(':')
                self.public_ports[port] = host_port
        self.links.extend(h.pop('links', []))
        self.unkown_docker_compose_items.update(h)

    def ensure_directories(self):
        """Create the direcoties, this container needs"""
        logging.debug("Creating Directories for DiwaContainer. self.projectdir: %s, self.volumedir: %s, self.host_log_dir: %s" % (self.project_dir, self.volume_dir, self.host_log_dir))
        chdir(self.project_dir)
        ensure_dirs(self.volume_dir)
        ensure_dirs(self.host_log_dir, mode=0777)

    @property
    def hash_for_docker_compose(self):
        """Return the hash to recreate the docker-compose.yml file"""
        self.__trim_volumes()
        h = dict(hostname=self.name,
                 image=self.image,
                 ports=self.public_ports_list,
                 links=list(sorted(set(self.links))),
                 volumes=list(sorted(set(self.volumes))),
                 expose=list(sorted(set(self.ports))),
                 environment=self.environment_list)
        for key, value in h.items():
            if is_empty(value):
                del(h[key])
        d = self.unkown_docker_compose_items.copy()
        d.update(h)
        return {self.name: d}

    def make_all_ports_public(self, straight=False):
        r = SystemRandom()
        for port in self.ports:
            host_port = port if straight else r.randrange(52000, 59000)
            self.public_ports[port] = host_port

    def remove_self_from_project(self, containers):
        """
        Remove all configurations the containers in the list, that at provided by this conainer
        :param containers: list of the containers still in the project
        :return:
        """
        pass

    def add_self_to_project(self, containers):
        """
        add all configurations the containers in the list, that at provided by this conainer
        :param containers: list of the containers existing in the project
        :return:
        """
        pass

    def removed_from_project(self, containers, container):
        """
        called when a container is removed
        :param containers: Containers still in the project
        :param container: Container, that has been removed
        :return:
        """
        pass

    def added_to_project(self, containers):
        """
        Called when a new container has been added to the project. The new container is the last
        in the list.
        :return: Containers existing in the project
        """
        pass

class ElkContainer(DiwaContainer):

    name = CONTAINERNAME_ELK
    image = 'sumpfgottheitlocal/common-diwa-elk'
    ports = [5601, ]
    description = "ELK Stack (Elasticsearch Logstash Kibana)"
    initial_logstash_port = 59000

    def __init__(self, project):
        super(self.__class__, self).__init__(ElkContainer.name, ElkContainer.image, project)
        self.volumes = [v for v in self.volumes if not v.startswith("%s:/logs" % self.host_log_dir)]
        self.volumes.extend(["%s/logstash.d:/etc/logstash.d" % self.volume_dir,
                        "%s/elastichsearchdata:/var/elasticsearch" % self.volume_dir,
                        "%s/:/logs" % ElkContainer._logs_dir])
        self.volumes = list(sorted(set(self.volumes)))

    def configure_container(self, container, port):
        if container.name != self.name:
            logging.info("Configuring new Container %s for usage with %s" % (container.name, self.name))
            container.links.append("%s:%s" % (self.name, self.name))
            container.environment['LOGSTASH_HOST'] = self.name
            container.environment['LOGSTASH_PORT'] = port

    def deconfigure_container(self, container):
        logging.debug("Remove configuration for container %s from container %s" % (self.name, container.name))
        container.environment.pop('LOGSTASH_HOST', None)
        container.environment.pop('LOGSTASH_PORT', None)
        len_links = len(container.links)
        container.links = [i for i in container.links if not i.endswith(':%s' % self.name)]
        return len_links != len(container.links)

    def add_self_to_project(self, containers):
        logging.info("Add container %s to project" % self.name)
        containers_to_recreate = []
        for i, container in enumerate(containers):
            self.configure_container(container, ElkContainer.initial_logstash_port + i)
            containers_to_recreate.append(container)
        return containers_to_recreate

    def added_to_project(self, containers):
        container = containers[-1]
        self.configure_container(container, ElkContainer.initial_logstash_port + len(containers))

    def remove_self_from_project(self, containers):
        """Remove the configurations for self from the containers. Return a list of containers,
        that needs to be recrearted"""
        logging.info("Remove container %s from project" % self.name)
        needs_recreate = []
        for container in containers:
            if self.deconfigure_container(container):
                needs_recreate.append(container)
        return needs_recreate

    def removed_from_project(self, containers, container):
        pass

class Postgres94Container(DiwaContainer):

    name = CONTAINERNAME_POSTGRES94
    image = 'sumpfgottheitlocal/common-diwa-postgres94'
    ports = [5432, ]
    description = 'PostgresQL 9.4'

    def __init__(self, project):
        super(self.__class__, self).__init__(Postgres94Container.name, Postgres94Container.image, project)
        self.volumes.extend(["%s/pgdata:/var/lib/postgresql/data" % self.volume_dir, ])
        self.volumes = list(sorted(set(self.volumes)))
        self.password = random_password()


    @property
    def password(self):
        return self.environment['POSTGRES_PASSWORD']

    @password.setter
    def password(self, password):
        self.environment['POSTGRES_PASSWORD'] = password

    def configure_container(self, container):
        if container.is_devel_container and container.name != self.name:
            logging.debug("Configure container %s for new %s container" % (container.name, self.name))
            container.links.append("%s:postgres" % self.name)
            container.environment['POSTGRES_HOST'] = 'postgres'
            container.environment['POSTGRES_PORT'] = self.ports[0]
            return True
        return False

    def deconfigure_container(self, container):
        """Deconfigure the given container. Return True, if this container needs to be recreated"""
        if container == self:
            return
        logging.debug("Remove configuration for container %s from container %s" % (self.name, container.name))
        container.environment.pop('POSTGRES_HOST', None)
        container.environment.pop('POSTGRES_PORT', None)
        len_links = len(container.links)
        container.links = [i for i in container.links if not i.endswith(':postgres')]
        return len_links != len(container.links)

    def add_self_to_project(self, containers):
        logging.info("Add container %s to project" % self.name)
        containers_to_recreate = []
        for container in containers:
            if self.configure_container(container):
                containers_to_recreate.append(container)
        return containers_to_recreate

    def added_to_project(self, containers):
        container = containers[-1]
        logging.info("Configuring new Container %s for usage with %s" % (container.name, self.name))
        self.configure_container(container)

    def remove_self_from_project(self, containers):
        """Remove the configurations for self from the containers. Return a list of containers,
        that needs to be recrearted"""
        logging.info("Remove container %s from project" % self.name)
        needs_recreate = []
        for container in containers:
            if self.deconfigure_container(container):
                needs_recreate.append(container)
        return needs_recreate


    def removed_from_project(self, containers, container):
        pass

class ApachePHPContainer(DiwaContainer):

    name = CONTAINERNAME_APACHEPHP
    image = 'sumpfgottheitlocal/diwa-apache-php'
    ports = [80, 443]
    description = 'Apache Webserver with mod_php'
    is_devel_container = True

    def __init__(self, project):
        super(self.__class__, self).__init__(ApachePHPContainer.name, ApachePHPContainer.image, project)
        self.volumes.extend(["%s/html:/var/www/html" % self.volume_dir, ])
        self.volumes = list(sorted(set(self.volumes)))
        self.samples_dir = join(dirname(abspath(__file__)), 'samples', self.name)

    @property
    def development_dir(self):
        return join(self.project_dir, self.volume_dir, 'html')

    @property
    def containers_without_sample(self):
        containers_with_sample = [chunk[0] for chunk in self.sample_chunks]
        return [container.name for container in self.project.containers if container.name not in containers_with_sample and container.name != self.name]

    @property
    def chunk_filenames(self):
        return ["%s.php.inc.jinja2" % chunk[0] for chunk in self.sample_chunks]

    @property
    def sample_chunks(self):
        includes = []
        for container in self.project.containers:
            include_candidate = join(self.samples_dir, "%s.php.inc.jinja2" % container.name)
            if isfile(include_candidate):
                includes.append((container.name, readfile(include_candidate)))
        return includes

    @property
    def sample_file(self):
        env = Environment(loader=FileSystemLoader(self.samples_dir))
        template = env.get_template("diwa.php.jinja2")
        return join(self.development_dir, 'diwa.php'), template.render(project=self.project, container=self)
